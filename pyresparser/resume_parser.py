import re
import os
import multiprocessing as mp
import io
import en_core_web_sm
import pprint
from spacy.matcher import Matcher
import utils
from datetime import datetime
from pytz import timezone
import nltk
from nltk.tag import StanfordNERTagger
import traceback
import sys
import pathlib
class ResumeParser(object):

    def __init__(
        self,
        resume,
        skills_file=None,
        custom_regex=None
    ):
        
        nlp = en_core_web_sm.load()
        
        self.__skills_file = skills_file
        self.__matcher = Matcher(nlp.vocab)
        self.__details = {
            'name': None,
            'email': None,
            'technical_skills': None,
            'phone_numbers':None,
            'non_technical_skills':None,
            'predicted_name':None,
            'date_parsed':None
            
            
        }
        self.__resume = resume
        if not isinstance(self.__resume, io.BytesIO):
            # ext = pathlib.Path(self.__resume).suffix
            ext = os.path.splitext(self.__resume)[1].split('.')[1]
        else:
            ext = self.__resume.name.split('.')[1]
        
        self.__text_raw = utils.extract_text(self.__resume, '.' + ext).encode('ascii', 'ignore').decode('ascii')  
        self.__text = ' '.join(self.__text_raw.split())
        self.__nlp = nlp(self.__text_raw)
        self.__noun_chunks = list(self.__nlp.noun_chunks)
        
        self.__get_basic_details()
        

    def get_extracted_data(self):
        return self.__details

    def camel_case_split(self, s):
        idx = list(map(str.isupper, s))
        # mark change of case
        l = [0]
        for (i, (x, y)) in enumerate(zip(idx, idx[1:])):
            if x and not y:  # "Ul"
                l.append(i)
            elif not x and y:  # "lU"
                l.append(i+1)
        l.append(len(s))
        # for "lUl", index of "U" will pop twice, have to filer it
        arr = [s[x:y] for x, y in zip(l, l[1:]) if x < y]
        if len(arr) > 1:
            return arr[0] + ' ' + arr[1]
        elif len(arr) > 0:
            return arr[0]
        else:
            return ''

    def clean_text_for_name(self, text):
        
        text_arr = text.split('\n')
        clean_text = ''
        for a in text_arr:
            if '  ' in a:
                x = a.replace('  ','|').replace(' ','').replace('|', ' ')
                clean_text = clean_text + x + '\n'
            else: 
                clean_text = clean_text + a + '\n'
        return clean_text

    def __get_basic_details(self):
        try:
            clean_raw_text = re.sub(r'\n\s*\n', '\n\n', self.__text_raw)
            resume_first_section = os.linesep.join(clean_raw_text.split(os.linesep)[:15])
            entities = utils.get_entities(resume_first_section.title().strip())
            name = utils.predict_name(entities)
            cleaned_text= self.clean_text_for_name(resume_first_section.title())
            predicted_name = None
            if name is None or not name or name == '':
                print('name2')
                entities = utils.get_entities(cleaned_text)
                predicted_name = self.camel_case_split(utils.predict_name(entities))
            if name is None or not name or name == '':
                print('name3')
                predicted_name = self.camel_case_split(utils.extract_name(resume_first_section, self.__nlp, matcher=self.__matcher))

            
            email = utils.extract_email(resume_first_section.lower())
            if email is None or not email or len(email) == 0:
                email = utils.predict_email(entities)                       
            if email is None or not email or len(email) == 0:
                email = utils.extract_email(self.__text_raw.lower())            
        
            skills = utils.extract_skills(
                        self.__nlp,
                        self.__noun_chunks,
                        self.__skills_file,
                        technical_skills = True
                    )
        
            non_tech_skills = utils.extract_skills(
                        self.__nlp,
                        self.__noun_chunks,
                        self.__skills_file,
                        technical_skills = False
                    )

            
            self.__details['name'] = name

            # extract email
            self.__details['email'] = email

            # extract skills
            self.__details['technical_skills'] = skills
            self.__details['non_technical_skills'] = non_tech_skills
            
            
            phone_numbers = utils.find_phone(resume_first_section)
            if phone_numbers is None or not phone_numbers or len(phone_numbers) ==0:
                phone_numbers = utils.find_phone(self.__text_raw)
                
            if phone_numbers is None or not phone_numbers or len(phone_numbers) ==0:
                phone_numbers = utils.find_phone(cleaned_text)
            
            self.__details['phone_numbers'] = phone_numbers        
            if predicted_name is not None:
                self.__details['name'] = predicted_name
                self.__details['predicted_name'] = True
            else:
                self.__details['predicted_name'] = False
            self.__details['date_parsed'] = datetime.now(timezone('UTC')).astimezone(timezone('Asia/Kolkata'))
            
            return
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
            return 


def resume_result_wrapper(resume):
    parser = ResumeParser(resume)
    return parser.get_extracted_data()


if __name__ == '__main__':
    pool = mp.Pool(mp.cpu_count())

    resumes = []
    data = []
    for root, directories, filenames in os.walk('resumes'):
        for filename in filenames:
            file = os.path.join(root, filename)
            resumes.append(file)

    results = [
        pool.apply_async(
            resume_result_wrapper,
            args=(x,)
        ) for x in resumes
    ]

    results = [p.get() for p in results]
