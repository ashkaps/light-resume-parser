#!/usr/bin/env python
# coding: utf8
"""Example of training an additional entity type

This script shows how to add a new entity type to an existing pre-trained NER
model. To keep the example short and simple, only four sentences are provided
as examples. In practice, you'll need many more — a few hundred would be a
good start. You will also likely need to mix in examples of other entity
types, which might be obtained by running the entity recognizer over unlabelled
sentences, and adding their annotations to the training set.

The actual training is performed by looping over the examples, and calling
`nlp.entity.update()`. The `update()` method steps through the words of the
input. At each word, it makes a prediction. It then consults the annotations
provided on the GoldParse instance, to see whether it was right. If it was
wrong, it adjusts its weights so that the correct action will score higher
next time.

After training your model, you can save it to a directory. We recommend
wrapping models as Python packages, for ease of deployment.

For more details, see the documentation:
* Training: https://spacy.io/usage/training
* NER: https://spacy.io/usage/linguistic-features#named-entities

Compatible with: spaCy v2.1.0+
Last tested with: v2.1.0
"""
from __future__ import unicode_literals
from __future__ import print_function
import re
import plac
import random
from pathlib import Path
import spacy
import json
import logging
import pandas as pd
from spacy.util import minibatch, compounding


# training data
# Note: If you're using an existing model, make sure to mix in examples of
# other entity types that spaCy correctly recognized before. Otherwise, your
# model might learn the new type, but "forget" what it previously knew.
# https://explosion.ai/blog/pseudo-rehearsal-catastrophic-forgetting


def convert_csv_to_spacy_training_data(filename):
    print()
    df = pd.read_csv(open(filename))
    l1 = []
    l2 = []
    for i in range(0,len(df['Keyword'])):
        l1.append(df['Keyword'][i])
        l2.append({"entities":[(0,len(df['Keyword'][i]), df['Label'][i])]})

    TRAIN_DATA = list(zip(l1,l2))
    print(len(TRAIN_DATA))
    return TRAIN_DATA




@plac.annotations(
    model=("Model name. Defaults to blank 'en' model.", "option", "m", str),
    new_model_name=("New model name for model meta.", "option", "nm", str),
    output_dir=("Optional output directory", "option", "o", Path),
    n_iter=("Number of training iterations", "option", "n", int),
)
def main(
#     model='en_core_web_sm',
    model = None,
    new_model_name="trained",
    output_dir='/Users/aishvaryakapoor/Documents/personal/freelancing/resparsers/resume_parser/resume-parser/pyresparser/training',
    n_iter=20
):
    test_new_model()
    
    TRAIN_DATA = convert_csv_to_spacy_training_data('/Users/aishvaryakapoor/Documents/personal/freelancing/resparsers/resume_parser/resume-parser/pyresparser/training_input_per.csv')

    """Set up the pipeline and entity recognizer, and train the new entity."""
    random.seed(0)
    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
        print("Loaded model '%s'" % model)
    else:
        nlp = spacy.blank("en")  # create blank Language class
        print("Created blank 'en' model")
    # Add entity recognizer to model if it's not in the pipeline
    # nlp.create_pipe works for built-ins that are registered with spaCy

    if "ner" not in nlp.pipe_names:
        print("Creating new pipe")
        ner = nlp.create_pipe("ner")
        nlp.add_pipe(ner, last=True)

    # otherwise, get it, so we can add labels to it
    else:
        ner = nlp.get_pipe("ner")
        

    # add labels
    for _, annotations in TRAIN_DATA:
        for ent in annotations.get('entities'):
            ner.add_label(ent[2])

    # if model is None or reset_weights:
    #     optimizer = nlp.begin_training()
    # else:
    #     optimizer = nlp.resume_training()
    move_names = list(ner.move_names)
    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "ner"]
    with nlp.disable_pipes(*other_pipes):  # only train NER
        optimizer = nlp.begin_training()
        # batch up the examples using spaCy's minibatch
        for itn in range(n_iter):
            print("Starting iteration " + str(itn))
            random.shuffle(TRAIN_DATA)
            losses = {}
            batches = minibatch(TRAIN_DATA, size=compounding(4.0, 32.0, 1.001))
            for batch in batches:
                text, annotations = zip(*batch)
                nlp.update(
                    text,  # batch of texts
                    annotations,  # batch of annotations
                    drop=0.2,  # dropout - make it harder to memorise data
                    sgd=optimizer,  # callable to update weights
                    losses=losses)
#                 print("Losses", losses)
            print(">>> Iteration Losses", losses)

    # test the trained model
    test_text = "Briq hired Aishvarya Kapoor to head india operation and automation vertical. He graduated from University of Petroleum and Enerdy Studies, Dehradun"
    doc = nlp(test_text)
    print("Entities in '%s'" % test_text)
    for ent in doc.ents:
        print(ent.text, ent.start_char, ent.end_char, ent.label_)
    

    # save model to output directory
    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        nlp.meta["name"] = new_model_name  # rename model
        nlp.to_disk(output_dir)
        print("Saved model to", output_dir)

        # test the saved model
        print("Loading from", output_dir)
        nlp2 = spacy.load(output_dir)
        # Check the classes have loaded back consistently
#         assert nlp2.get_pipe("ner").move_names == move_names
        
        doc2 = nlp2(test_text)
        for ent in doc.ents:
            print(ent.text, ent.start_char, ent.end_char, ent.label_)

def test_new_model():
    test_text = "Briq hired Aishvarya Kapoor to head india operation and automation vertical. He graduated from University of Petroleum and Enerdy Studies, Dehradun"
    nlp2 = spacy.load("/Users/aishvaryakapoor/Documents/personal/freelancing/resparsers/resume_parser/resume-parser/pyresparser/training")
        # Check the classes have loaded back consistently
#         assert nlp2.get_pipe("ner").move_names == move_names
    doc2 = nlp2(test_text)
    for ent in doc2.ents:
        print(doc2.ents)
        print(ent.text, ent.start_char, ent.end_char, ent.label_)


if __name__ == "__main__":
    plac.call(main)
