from nltk.corpus import stopwords

# Omkar Pathak
NAME_PATTERN = [{'POS': 'PROPN'}, {'POS': 'PROPN'}]

# Education (Upper Case Mandatory)
#!/usr/bin/python
# -*- coding: utf-8 -*-
EDUCATION = [
    'BE',
    'B.E.',
    'B.E',
    'BS',
    'B.S',
    'ME',
    'M.E',
    'M.E.',
    'MS',
    'M.S',
    'BTECH',
    'MTECH',
    'BBA',
    'M.P.T.',
    'PGD',
    'B.C.J',
    'PGD In Engineering',
    'B.A. LLB',
    'M.A.',
    'LL.B.',
    'M.D.',
    'M.B.B.S.',
    'M.D.S.',
    'Bdes.',
    'M.Sc.',
    'Other Bachelors',
    'Integrated Degree In Pharmacy',
    'PGP',
    'Other Masters',
    'Diploma in Pharmacy',
    'M.B.B.S.',
    'B.E.M.',
    'B.Ed.',
    'MMS',
    'M.T.M',
    'B.M.M.',
    'B.F.A',
    'B.Sc.',
    'MBA',
    'B.H.M.',
    'B.A.',
    'B.Sc.',
    'PGD',
    'M.D.',
    'M.Phil.',
    'B.A. LLB',
    'B.Com LLB',
    'B.T.M',
    'B.Com.',
    'M.F.A',
    'B.A.',
    'M.E /Mtech.',
    'B.C.J',
    'B.Sc LLB',
    'B.Pharm.',
    'M.Arch',
    'BE',
    'B.E /Btech',
    'B.H.M.S',
    'BBA LLB',
    'M.Des.',
    'B.D.S',
    'MBA',
    'B.F.D.',
    'M.Sc.',
    'B.E /Btech',
    'Bdes.',
    'B.F.I.A.',
    'B.Pharm.',
    'Other Masters [Science]',
    'Diploma in Elementary Education',
    'LL.B.',
    'M.Pharm.',
    'M.Com.',
    'B.H.M.',
    'P.G.D.F.D.',
    'Other Bachelors [Science]',
    'M.H.M.',
    'M.Ed.',
    'LL.M.',
    'B.Arch',
    'Ph.D',
    'M.H.M.',
    'B.P.R.',
    'M.A.',
    'BBA',
    'M.C.J',
    'B.F.D.',
    'M.S.',
    'Executive MBA',
    'LL.M.',
    'SSC',
    'HSC',
    'CBSE',
    'ICSE',
    'X',
    'XII',
    ]


NOT_ALPHA_NUMERIC = r'[^a-zA-Z\d]'

NUMBER = r'\d+'

# For finding date ranges
MONTHS_SHORT = r'''(jan)|(feb)|(mar)|(apr)|(may)|(jun)|(jul)
                   |(aug)|(sep)|(oct)|(nov)|(dec)'''
MONTHS_LONG = r'''(january)|(february)|(march)|(april)|(may)|(june)|(july)|
                   (august)|(september)|(october)|(november)|(december)'''
MONTH = r'(' + MONTHS_SHORT + r'|' + MONTHS_LONG + r')'
YEAR = r'(((20|19)(\d{2})))'

STOPWORDS = set(stopwords.words('english'))

RESUME_SECTIONS_PROFESSIONAL = [
                    'experience',
                    'education',
                    'interests',
                    'professional experience',
                    'publications',
                    'skills',
                    'certifications',
                    'objective',
                    'career objective',
                    'summary',
                    'leadership'
                ]

RESUME_SECTIONS_GRAD = [
                    'accomplishments',
                    'experience',
                    'education',
                    'interests',
                    'projects',
                    'professional experience',
                    'publications',
                    'skills',
                    'certifications',
                    'objective',
                    'career objective',
                    'summary',
                    'leadership',
                    'institution',
                    'board'
                    
                ]
