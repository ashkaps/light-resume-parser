from resume_parser import ResumeParser 
import os
import csv


all = list()
all_input = os.listdir('/Users/aishvaryakapoor/Downloads/resume 2')
print(str(len(all_input)) + ' files')
file_cnt = 1
for filename in all_input:
    try:
        a = ResumeParser('/Users/aishvaryakapoor/Downloads/resume 2/' + filename).get_extracted_data()
        a['filename'] = filename
        all.append(a)
        print(str(file_cnt) + ' out of ' + str(len(all_input)))
        
    except Exception as e:
        print(e)
        print(filename)
    file_cnt = file_cnt + 1


keys = all[0].keys()
with open('/Users/aishvaryakapoor/Downloads/resume 2/resume_output.csv', 'w', newline='')  as output_file:
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    dict_writer.writerows(all)