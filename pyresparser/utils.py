import phonenumbers
import io
import os
import re
import nltk
import pandas as pd
import docx2txt
from datetime import datetime
from dateutil import relativedelta
import constants as cs
from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfparser import PDFSyntaxError
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.parse.corenlp import CoreNLPParser
import logging
import pickle
import textract
import configurations  as regex
from datetime import date
import dirpath
import PyPDF2
import datefinder
import csv
from tika import parser   

def extract_tika(pdf_path):
    parsed_pdf = parser.from_file(pdf_path, service='text') 
    data = parsed_pdf['content']  
    return data
    # return extract_pdf(pdf_path)


def extract_text_from_pdf(pdf_path):
    '''
    Helper function to extract the plain text from .pdf files

    :param pdf_path: path to PDF file to be extracted (remote or local)
    :return: iterator of string of extracted text
    '''
    # https://www.blog.pythonlibrary.org/2018/05/03/exporting-data-from-pdfs-with-python/
    if not isinstance(pdf_path, io.BytesIO):
        # extract text from local pdf file
        with open(pdf_path, 'rb') as fh:
            try:
                for page in PDFPage.get_pages(
                        fh,
                        caching=True,
                        check_extractable=True
                ):
                    resource_manager = PDFResourceManager()
                    fake_file_handle = io.StringIO()
                    converter = TextConverter(
                        resource_manager,
                        fake_file_handle,
                        codec='utf-8',
                        laparams=LAParams()
                    )
                    page_interpreter = PDFPageInterpreter(
                        resource_manager,
                        converter
                    )
                    page_interpreter.process_page(page)

                    text = fake_file_handle.getvalue()
                    yield text

                    # close open handles
                    converter.close()
                    fake_file_handle.close()
            except PDFSyntaxError:
                return
    else:
        # extract text from remote pdf file
        try:
            for page in PDFPage.get_pages(
                    pdf_path,
                    caching=True,
                    check_extractable=True
            ):
                resource_manager = PDFResourceManager()
                fake_file_handle = io.StringIO()
                converter = TextConverter(
                    resource_manager,
                    fake_file_handle,
                    codec='utf-8',
                    laparams=LAParams()
                )
                page_interpreter = PDFPageInterpreter(
                    resource_manager,
                    converter
                )
                page_interpreter.process_page(page)

                text = fake_file_handle.getvalue()
                yield text

                # close open handles
                converter.close()
                fake_file_handle.close()
        except PDFSyntaxError:
            return


def extract_text_from_docx(doc_path):
    '''
    Helper function to extract plain text from .docx files

    :param doc_path: path to .docx file to be extracted
    :return: string of extracted text
    '''
    try:
        temp = docx2txt.process(doc_path)
        text = [line.replace('\t', ' ') for line in temp.split('\n') if line]
        return ' '.join(text)
    except KeyError:
        return ' '


def extract_text_from_doc(doc_path):
    '''
    Helper function to extract plain text from .doc files

    :param doc_path: path to .doc file to be extracted
    :return: string of extracted text
    '''
    try:
        text = textract.process(doc_path).decode('utf-8')
        return text
    except KeyError:
        return ''


def extract_text(file_path, extension):
    '''
    Wrapper function to detect the file extension and call text
    extraction function accordingly

    :param file_path: path of file of which text is to be extracted
    :param extension: extension of file `file_name`
    '''
    text = ''
    if extension == '.pdf':
        
        for page in extract_text_from_pdf(file_path):
            text += ' ' + page
        if len(text.strip()) < 50:
            text = extract_tika(file_path)
    elif extension == '.docx':
        text = extract_text_from_docx(file_path)
    elif extension == '.doc':
        text = extract_text_from_doc(file_path)
    return text

def extract_email(text):
    '''
    Helper function to extract email id from text

    :param text: plain text extracted from resume file
    '''
    emails = re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", text)
    if len(emails) > 0:
        return list(set(emails))
    else: return None
       
def extract_email_addresses(string):
    try:
        list_email = set()
        r = re.compile(r'[\w\.-]+@[\w\.-]+')
        for email in r.findall(string):
            list_email.add(email)
    except Exception:
        return None
def get_continuous_chunks(tagged_sent):
    continuous_chunk = []
    current_chunk = []

    for token, tag in tagged_sent:
        if tag != "O":
            current_chunk.append((token, tag))
        else:
            if current_chunk: # if the current chunk is not empty
                continuous_chunk.append(current_chunk)
                current_chunk = []
    # Flush the final current_chunk into the continuous_chunk, if any.
    if current_chunk:
        continuous_chunk.append(current_chunk)
    return continuous_chunk

pos_tagger = CoreNLPParser(url='http://localhost:9001', tagtype='ner')

def get_entities(text):
    # print(text)
    person_list = list()
    try: 
        named_entities = get_continuous_chunks(pos_tagger.tag((text.title().split())))
        
        named_entities_str_tag = [(" ".join([token for token, tag in ne]), ne[0][1]) for ne in named_entities]
        # print(named_entities_str_tag)
        return named_entities_str_tag
    except Exception:
        return person_list

def predict_entity_value(named_entities_str_tag, tag_name):
    result = list()
    for tag in named_entities_str_tag:
            if tag[1] == tag_name:
                nm = tag[0].split(' ')
                name = ''
                if len(nm) > 1:
                    name = nm[0] + ' ' + nm[1]
                elif len(nm) == 1:
                    name = nm[0] 
                if len(name) > 0:
                    result.append(name)
                    

    return result

def predict_name(named_entities_str_tag):
    name = ''
    result = predict_entity_value(named_entities_str_tag, "PERSON")
    if len(result) > 0:
        if len(result) > 1:
            name = result[0] + ' ' + result[1]
        if len(result) > 0:
            name = result[0]
    # if len(name) > 0:
    #     name_arr = name.split(' ')
    #     if len(name_arr) > 1:
    #         name = name_arr[0] + ' ' + name_arr[1]
    #     if len(name_arr) > 0:
    #         name = name_arr[0]

    return name

def predict_email(named_entities_str_tag):
    email = predict_entity_value(named_entities_str_tag, "EMAIL")
    if email is not None and len(email) >0:
        return email[0].lower()
    return ''

def predict_nationality(named_entities_str_tag):
    nationality = predict_entity_value(named_entities_str_tag, "NATIONALITY")
    if nationality is not None:
        return nationality.title()
    return ''

def fetch_name(resume_text):
    tokenized_sentences = nltk.sent_tokenize(resume_text)
    full_name = ''
    count = 0
    for sentence in tokenized_sentences:
        for chunk in \
            nltk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(sentence),
                          tagset='universal')):
            if hasattr(chunk, 'label'):  # and chunk.label() == 'PERSON':
                chunk = chunk[0]
            (name, tag) = chunk
            if tag == 'NOUN' and count < 2:
                full_name = full_name + name + ' '
                count = count + 1
    return full_name.strip()

def extract_name(text, nlp_text, matcher):   
    full_name = ''
    try:
        stop = stopwords.words('english')
        document = ' '.join([i for i in text.split() if i not in stop])
        sentences = nltk.sent_tokenize(document)
        sentences = [nltk.word_tokenize(sent) for sent in sentences]
        sentences = [nltk.pos_tag(sent) for sent in sentences]
        
        
        pattern = [cs.NAME_PATTERN]
    
        matcher.add('NAME', None, *pattern)
    
        matches = matcher(nlp_text)
    
        for _, start, end in matches:
            span = nlp_text[start:end]
            if 'name' not in span.text.lower():
                full_name = span.text
    except Exception:
        full_name = ''

    if full_name == '':
        full_name = fetch_name(text)

    
    return full_name.title()


def extract_skills(nlp_text, noun_chunks, skills_file=None, technical_skills = True):
    '''
    Helper function to extract skills from spacy nlp text

    :param nlp_text: object of `spacy.tokens.doc.Doc`
    :param noun_chunks: noun chunks extracted from nlp text
    :return: list of skills extracted
    '''
    tokens = [token.text for token in nlp_text if not token.is_stop]
    if not skills_file:
        if technical_skills:
            data = pd.read_csv(
                os.path.join(os.path.dirname(__file__), 'technical_skills.csv')
            )
        else :
            data = pd.read_csv(
                os.path.join(os.path.dirname(__file__), 'skills.csv')
            )
        
        matrix2 = data[data.columns[0]].to_numpy()
        skills = matrix2.tolist()
    else:
        data = pd.read_csv(skills_file)
        skills = list(data.columns.values)
    skillset = []
    # check for one-grams
    for token in tokens:
        if token.lower() in skills:
            skillset.append(token)

    # check for bi-grams and tri-grams
    for token in noun_chunks:
        token = token.text.lower().strip()
        if token in skills:
            skillset.append(token)

    return [i.capitalize() for i in set([i.lower() for i in skillset])]


def cleanup(token, lower=True):
    if lower:
        token = token.lower()
    return token.strip()


def find_phone(text):
    # print(text)
    # text = re.sub(r'[^A-Za-z0-9 \n]+', '',text)
    all_phone = list()
    try:
        for ph in re.findall(r'(\d{3,}[-\.\s]??\d{3,}[-\.\s]??\d{3,}|\(\d{3,}\)\s*\d{3,}[-\.\s]??\d{3,}|\d{3,}[-\.\s]??\d{3,})', text):
            all_phone.append(ph.strip())
    except Exception as e:
        print(e)
    print(all_phone)
    if len(all_phone) == 0:
        try:
            for match in phonenumbers.PhoneNumberMatcher(text, "US"):
                all_phone.append(match)
        except Exception as e:
            print(e)
    cleaned_list = set()
    for ph in all_phone:
        clean_ph = ''.join(filter(str.isdigit, ph))
        ph_len = len(clean_ph)
        if ( ph_len == 7 or ph_len >= 10 ) and ph_len <13:
            cleaned_list.add(clean_ph)
    final_list = list()
    if len(cleaned_list) > 0:
        phno = list(cleaned_list)[0]
        if len(phno) > 10:
            phno = '+' + phno
        final_list.append(phno)
    
    return final_list

def fetch_skills(cleaned_resume):
    with open(dirpath.PKGPATH + '/data/skills/skills', 'rb') as fp:
        skills = pickle.load(fp)

    skill_set = []
    for skill in skills:
        skill = ' ' + skill + ' '
        if skill.lower() in cleaned_resume:
            skill_set.append(skill)
    return skill_set

