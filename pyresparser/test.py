from tika import parser   
  

def clean_text_for_name(text):
    text_arr = text.strip().split('\n')
    clean_text = ''
    for a in text_arr:
        if '  ' in a:
            x = a.replace('  ','|').replace(' ','').replace('|', ' ')
            clean_text = clean_text + x + '\n'
        else: 
            clean_text = clean_text + a + '\n'
    print('CLEANED TEXT : ' + clean_text)
    return clean_text
# opening pdf file 
parsed_pdf = parser.from_file("/Users/aishvaryakapoor/Downloads/Resume_bobbybee. (1).pdf", service='text') 
  
# saving content of pdf 
# you can also bring text only, by parsed_pdf['text']  
# parsed_pdf['content'] returns string  
data = parsed_pdf['content']  
clean_text_for_name(data)
# Printing of content  
print(data) 
  
# <class 'str'> 
print(type(data))




