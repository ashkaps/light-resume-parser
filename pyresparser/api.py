import os
import urllib.request
from app import app
from flask import Flask, request, redirect, jsonify
from werkzeug.utils import secure_filename

from resume_parser import ResumeParser 
import tempfile
import json


ALLOWED_EXTENSIONS = set(['docx', 'pdf', 'doc'])
TOKEN = 'U2FsdGVkX19kdHaXMIUUd9ddmA/4lBmtpHs6UsjEMmbWo03Nr/Jly5E1RmqY054S'
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def auth(req):
    # print(str(req))
    if 'auth_token' in req:
        token = req['auth_token']
    else:
        raise Exception('Missing auth_token. You dont have access for this API')
    if token == TOKEN:
        return True
    else:
        raise Exception('Incorrect auth_token. You dont have access for this API')

@app.route('/parse_resume', methods=['GET'])
def get():
    auth(request.headers)
    resp = jsonify({'message' : 'Please use POST to send resume'})
    resp.status_code = 200
    return resp
    
@app.route('/parse_resume', methods=['POST'])
def upload_file():
    auth(request.headers)
    if 'file' not in request.files:
        resp = jsonify({'message' : 'No file part in the request'})
        resp.status_code = 400
        return resp
    file = request.files['file']
    if file.filename == '':
        resp = jsonify({'message' : 'No file selected for uploading'})
        resp.status_code = 400
        return resp
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(filename)
        a = ResumeParser(filename).get_extracted_data()
        os.remove(os.path.join(filename))
        return jsonify(a)
    else:
        resp = jsonify({'message' : 'Allowed file types are docx, doc, pdf'})
        resp.status_code = 400
        return resp

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')